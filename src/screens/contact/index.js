import React , {Component} from "react";

import {Text, View, StyleSheet} from "react-native";

export default class Contact extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Text>I am From Contact Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"green"
    }
});