import React , {Component} from "react";

import {Text, View, StyleSheet, Button} from "react-native";

import Modal from "../../component/modal";

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
        
        }
    }

    render(){
        return (
            <>
            <View style={styles.container}>
                <Text>I am From Home Screen</Text>
                <Button
                    title="Open Modal"
                    onPress={()=>this.Modal.open()}
                />
            </View>
            <Modal
                title="Home"
                onRef = {ref => this.Modal = ref}
                Description = "I am from Home screen"
                okbtn = {true}
                cnlbtn = {true}
                onAction = {()=>this.props.navigation.navigate('about')}
            />
            </>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"blue"
    }
});