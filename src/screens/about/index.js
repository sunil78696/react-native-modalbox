import React , {Component} from "react";

import {Text, View, StyleSheet} from "react-native";

export default class About extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Text>I am From About Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"red"
    }
});