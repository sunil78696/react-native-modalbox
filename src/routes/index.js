import {createAppContainer} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";


import Home from "../screens/home";
import About from "../screens/about";
import Contact from "../screens/contact";

const routes = createStackNavigator({
    home : Home,
    about : About,
    contact : Contact
},
{
    initialRouteName:"home"
});

export default createAppContainer(routes);