import React , {Component} from "react";

import {Text, View, StyleSheet, Button} from "react-native";

import Modal from "react-native-modalbox";

export default class MyModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen: true
        }
    }
    componentDidMount(){
        this.props.onRef(this.refs.Modal); 
    }

    render(){
        return (
            <Modal 
                style={styles.modal}
                position="center" 
                isOpen={this.state.isOpen}
                ref = {"Modal"}
                backdrop={false}
                swipeToClose={true}
                onOpened={()=>alert("modal open")}
                >
                <Text style={styles.text}>
                    {this.props.title}
                </Text>
                <Text style={{fontSize:16}}>
                    {this.props.Description}
                </Text>
                {this.props.okbtn && <Button 
                    title="Ok"
                    onPress={()=>this.props.onAction()}
                />
                }
                {this.props.cnlbtn && 
                    <Button 
                    title="Cancel"
                    onPress={()=>this.refs.Modal.close()}
                    />
                }
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"red"
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        width: 300
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    
    text: {
        color: "black",
        fontSize: 22
    }
});